import React from 'react'
import Nav from './components/shared/nav/Nav'
import Portfolio from './components/pages/portfolio/Portfolio'
import About from './components/pages/about/About'
import Contact from './components/pages/contact/Contact'
import Login from './components/pages/Login'
import Footer from './components/shared/footer/Footer'
import Listing from './components/pages/listing/Listing'
import User from './components/pages/User'
import PrivateRoute from './components/authentication/PrivateRoute'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

function App() {
  return (
    <Router>            
        <Nav />
        <Switch>
          <Route exact path="/" component={Portfolio} />
          <Route exact path="/about" component={About} />
          <Route exact path="/contact" component={Contact} />  
          <Route exact path="/login" component={Login} />  
          <Route exact path="/user" component={User} />
          <PrivateRoute path="/submissions">
            <Listing />
          </PrivateRoute>     
        </Switch>
        <Footer />         
    </Router>
  );
}

export default App;
