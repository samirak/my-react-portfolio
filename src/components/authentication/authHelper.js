export const parseJwt = (token) => {
  try {
    return JSON.parse(atob(token.split('.')[1]))
  }
  catch (err) {
    return false
  }
}

const isAuthenticated = () => {
  try {
    return parseJwt(sessionStorage.getItem('token'))
  }
  catch (err) {
    console.log(err)
    return false
  }
}

export default isAuthenticated