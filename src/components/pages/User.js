import React, { useState } from "react"
import "./contact/contact.css"

function User() {
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")  

  const formSubmit = async (event) => {
    event.preventDefault()
    const response = await fetch('http://localhost:5000/users', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin':'*'
      },
      body: JSON.stringify({name, email, password})
    })
    const payload = await response.json()
    if (response.status >= 400) {
      alert(`Oops! Error: ${payload.message} for fields: ${payload.invalid.join(",")}`)
    }
    else {      
      alert(`Congrats! Submission submitted with id: ${payload.id}`)
    }
  }

  return (
    <div>      
        <div className="logo">     
          <div className="logo-text">
            <h1>Create a new user</h1>
          </div>          
        </div>      
      
        <div className="contactWrapper">
          {!alert && 
            <div className="warningMsgWrapper">               
            </div>
          }
          {alert && 
            <div className="successMsgWrapper">               
            </div>
          }
          <form name="contactForm" onSubmit={formSubmit}>
              <div className="error">
                  <p className="message"></p>
              </div>
              <div>
                <div className="field">
                  <label for="nameEntry">Name</label>
                  <input type="text" name="name" id="nameEntry"
                          placeholder="Name" 
                          maxLength="50" 
                          value={name}                          
                          onChange={e => setName(e.target.value)}
                          required />
                </div>                                                         
                <div className="field">
                    <label for="emailEntry">Email</label>
                    <input type="email" name="email" id="emailEntry"
                            placeholder="Email" 
                            value={email} 
                            onChange={e => setEmail(e.target.value)}
                            required />
                </div>  
                <div className="field">
                    <label for="passwordEntry">Password</label>
                    <input type="password" name="password" id="passwordEntry"
                            placeholder="Password" 
                            value={password} 
                            onChange={e => setPassword(e.target.value)} 
                            required />
                </div>          
                                                             
                <input type="submit" id="submitButton" 
                        value="Submit" 
                        className="submitBtn" />
              </div>              
          </form>
        </div>
      
    </div>
  );
}

export default User;