import React from "react"
import "./portfolio.css"
import Logo from "../../logo/Logo"

class Portfolio extends React.Component {
  render() {
    return (
      <div>
        <section>
          <div className="logo">                    
              <Logo />
              <div className="logo-text">
                  <h1>Samira&#8217; Portfolio</h1>
              </div>
          </div>
        </section>
            
            <section>
                <div className="content-wrapper">
                    <div className="row">
                        <div className="column">
                            <div className="portfolio-column-content">
                                <a name="project-name-01" href="index.html">
                                    <img
                                        src="images/sample-4.jpg"
                                        alt="1"
                                    />
                                </a>
                                <div className="portfolio-overlay">Project title</div>
                            </div>
                        </div>
                        <div className="column">
                            <div className="portfolio-column-content">
                                <a name="project-name-02" href="index.html">
                                    <img
                                        src="images/sample-2.jpg"
                                        alt="2"
                                    />
                                </a>
                                <div className="portfolio-overlay">Project title</div>
                            </div>
                        </div>
                        <div className="column">
                            <div className="portfolio-column-content">
                                <a name="project-name-03" href="index.html">
                                    <img
                                        src="images/sample-3.jpg"
                                        alt="3"
                                    />
                                </a>
                                <div className="portfolio-overlay">Project title</div>
                            </div>
                        </div>
                    
                        <div className="column">
                            <div className="portfolio-column-content">
                                <a name="project-name-04" href="index.html">
                                    <img
                                        src="images/sample-5.jpg"
                                        alt="4"
                                    />
                                </a>
                                <div className="portfolio-overlay">Project title</div>
                            </div>
                        </div>
                        <div className="column">
                            <div className="portfolio-column-content">
                                <a name="project-name-05" href="index.html">
                                    <img
                                        src="images/sample-3.jpg"
                                        alt="5"
                                    />
                                </a>
                                <div className="portfolio-overlay">Project title</div>
                            </div>
                        </div>
                        <div className="column">
                            <div className="portfolio-column-content">
                                <a name="project-name-06" href="index.html">
                                    <img
                                        src="images/sample-1.jpg"
                                        alt="6"
                                    />
                                </a>
                                <div className="portfolio-overlay">Project title</div>
                            </div>
                        </div>
                        <div className="column">
                            <div className="portfolio-column-content">
                                <a name="project-name-01" href="index.html">
                                    <img
                                        src="images/sample-4.jpg"
                                        alt="7"
                                    />
                                </a>
                                <div className="portfolio-overlay">Project title</div>
                            </div>
                        </div>
                        <div className="column">
                            <div className="portfolio-column-content">
                                <a name="project-name-02" href="index.html">
                                    <img
                                        src="images/sample-2.jpg"
                                        alt="8"
                                    />
                                </a>
                                <div className="portfolio-overlay">Project title</div>
                            </div>
                        </div>
                        <div className="column">
                            <div className="portfolio-column-content">
                                <a name="project-name-03" href="index.html">
                                    <img
                                        src="images/sample-3.jpg"
                                        alt="9"
                                    />
                                </a>
                                <div className="portfolio-overlay">Project title</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
      </div>
    )
  }
}


export default Portfolio
