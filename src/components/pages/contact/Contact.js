import React, { useState } from "react"
import "./contact.css"
import Logo from "../../logo/Logo"

function Contact() {
  const [name, setName] = useState()
  const [email, setEmail] = useState()
  const [phoneNumber, setPhoneNumber] = useState()
  const [content, setContent] = useState()

  const formSubmit = async (event) => {
    event.preventDefault()
    const response = await fetch('http://localhost:5000/contact_form/entries', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin':'*'
      },
      body: JSON.stringify({name, email, phoneNumber, content})
    })
    const payload = await response.json()
    if (response.status >= 400) {
      alert(`Oops! Error: ${payload.message} for fields: ${payload.invalid.join(",")}`)
    }
    else {
      alert(`Congrats! Submission submitted with id: ${payload.id}`)
    }
  }

  return (
    <div>
      
        <div className="logo">                    
          <Logo />
          <div className="logo-text">
            <h1>Send me a message!</h1>
          </div>
          <div className="contactSendMessage">
            <p>Email:</p>
            <p><a name="myEmail" href="mailto:samira.khakshoor@gmail.com">samira.khakshoor@gmail.com</a></p>
          </div>
        </div>
      
      
        <div className="contactWrapper">
          <form name="contactForm" onSubmit={formSubmit}>
              <div className="error">
                  <p className="message"></p>
              </div>
              <div>
                <div className="field">
                  <label for="nameEntry">Name</label>
                  <input type="text" name="name" id="nameEntry"
                          placeholder="Name" 
                          maxLength="50" 
                          value={name}
                          onChange={e => setName(e.target.value)}
                          required />
                  
                </div>                                                         
                <div className="field">
                    <label for="emailEntry">Email</label>
                    <input type="email" name="email" id="emailEntry"
                            placeholder="Email" 
                            value={email} 
                            onChange={e => setEmail(e.target.value)}
                            required />
                    
                </div>  
                <div className="field">
                    <label for="phoneEntry">Phone number</label>
                    <input type="phone" name="phoneNum" id="phoneEntry"
                            placeholder="Phone number" 
                            value={phoneNumber} 
                            onChange={e => setPhoneNumber(e.target.value)} 
                            required />
                    
                </div>                                               
                <div className="field">
                    <label for="messageEntry">Message</label>
                    <textarea name="message" id="messageEntry"
                              placeholder="Leave me your thoughts..."
                              value={content}
                              onChange={e => setContent(e.target.value)}
                              required>
                    </textarea>                    
                    {/* <span className="errorMessage">Required</span> */}
                </div>                                              
                <input type="submit" id="submitButton" 
                        value="Submit" 
                        className="submitBtn" />
              </div>              
          </form>

          {/* <div className="successMsg"> 
            <span className="icon success"></span>
            <h3>Your Message has been sent.</h3>
          </div> */}
        </div>
      
    </div>
  );
}

export default Contact;
