import React, { useState } from "react"
import "./contact/contact.css"
import Warning from "../shared/svg/Warning"
import { useHistory, useLocation } from 'react-router-dom'

function Login() {
  let history = useHistory()
  let location = useLocation()
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [auth, setAuth] = useState(true)

  const loginSubmit = async (event) => {

    event.preventDefault()
    const response = await fetch('http://localhost:5000/auth', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin':'*'
      },
      body: JSON.stringify({email, password})
    })
    const payload = await response.json()
    if (response.status >= 400) {
      setAuth(false)
    }
    else {
      sessionStorage.setItem('token', payload.token)

      let { from } = location.state || { from: { pathname: "/" } }
      history.replace(from)
    }
  }

  return (
    <div>      
      <div className="logo">          
        <div className="logo-text">
          <h1>Login</h1>
        </div>          
      </div>      
      
        <div className="contactWrapper">
          {!auth && 
            <div className="warningMsgWrapper"> 
              <Warning />
              <h2 className="warningMsg">Invalid credentials, please try again</h2>
            </div>
          }
          <form name="contactForm" onSubmit={loginSubmit}>
              <div className="error">
                  <p className="message"></p>
              </div>
              <div>
                <div className="field">
                  <label for="usernameEntry">Username</label>
                  <input type="text" name="name" id="usernameEntry"
                          placeholder="Username"                            
                          value={email}
                          onChange={e => setEmail(e.target.value)}
                          required />
                  
                </div>                                                         
                <div className="field">
                    <label for="passwordEntry">Password</label>
                    <input type="password" name="password" id="passwordEntry"
                            placeholder="Password" 
                            value={password} 
                            onChange={e => setPassword(e.target.value)}
                            required />
                    
                </div> 
                                                             
                <input type="submit" id="signInButton" 
                        value="Sign in" 
                        className="submitBtn" />
              </div>              
          </form>          
        </div>
      
    </div>
  );
}

export default Login
