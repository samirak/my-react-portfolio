import React, { useEffect, useState } from 'react'
import parseJwt from '../../authentication/authHelper'
import { useHistory } from "react-router-dom";
import "./listing.css"


const Listing = () => {
    let history = useHistory();
    const token = sessionStorage.getItem('token')
    const email = parseJwt(token).reqEmail
    const [listing, setListing] = useState([])
    
    const logout = event => {
      event.preventDefault()
      sessionStorage.removeItem('token')
      history.push("/login")
    }

    const newUser = event => {
      event.preventDefault()
      history.push("/user")
  }

    useEffect(() => {
        const getData = async () => {
            const response = await fetch('http://localhost:5000/contact_form/entries', {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            const data = await response.json()
            setListing(data)
        }
        getData()
    }, [token])

    return (
      <div>            
        <div className="logo">
            <div className="logo-text">
              <h1>Listings for user {email}</h1>                     
            </div>  
        </div>        
      
        <div className="content-wrapper">       
          
        <table className="listing-table">
          <thead>
            <tr className="listing-table-row">
              <th className="listing-table-header">ID</th>
              <th className="listing-table-header">Name</th>
              <th className="listing-table-header">Phone Number</th>
              <th className="listing-table-header">Email</th>
            </tr>
          </thead>
          <tbody>
            {listing.length === 0 &&
                <tr className="listing-table-row">
                  <td colSpan="4"><i>No listings found</i></td>
                </tr>
            }
            {listing.length > 0 &&
                listing.map(entry => <tr className="listing-table-row">
                                        <td>{entry.id}</td>
                                        <td>{entry.name}</td>
                                        <td>{entry.phoneNumber}</td>
                                        <td>{entry.email}</td>
                                      </tr>)
            }
          </tbody>
        </table>

        <div className="listing-action">
            <input name="logout" type="submit" onClick={logout} value="Logout" className="listingBtn" />
            <input name="newUser" type="submit" onClick={newUser} value="Create a new user" className="listingBtn"/>
        </div>
        </div>
      </div>
    )
}

export default Listing