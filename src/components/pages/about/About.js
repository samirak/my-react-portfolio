import React from "react"
import "./about.css"
import Logo from "../../logo/Logo"

function About() {
  return (
    <div>      
        <div class="logo">                    
            <Logo />
            <div class="aboutMe">
              <h1>Samira&nbsp;Khakshoor</h1>  
              <h3>Full&nbsp;Stack&nbsp;Developer</h3>                      
            </div>  
        </div>        
      
        <div class="content-wrapper introduction">
          <p>Lorem ipsum dolor sit amet, etiam intellegam repudiandae eum ea, mea ut quaeque reformidans. 
              Pri ex dicant aperiri deterruisset. Ei cum dolor pertinacia, duo veri oporteat ut. 
              Adipisci vituperatoribus ei vix, his an tale nonumes. Sea an populo intellegebat.</p>

          <p>Ex pro iisque abhorreant adversarium, partem copiosae at cum. Vim cu quem diam, nemore 
              habemus propriae vel id. Eum ne dicat nostro. Blandit intellegebat qui no, ut mea ludus 
              labitur. Eam ea assentior disputationi, qui ne eros erroribus interpretaris. Enim posse 
              assentior eam ei, quot possim placerat eum ei.</p>
              
          <p>Et vide ridens iracundia pri, eam ne quidam appareat. Dolores abhorreant mel ad. Usu ne eripuit 
              lobortis. An sed nonumy populo labitur, sed lorem putant in. Ex accusam deserunt voluptatibus 
              per, pri ne cibo consulatu. Partem admodum ea nec, quodsi intellegebat eam ut. </p>

          <p>Lorem ipsum dolor sit amet, etiam intellegam repudiandae eum ea, mea ut quaeque reformidans. 
              Pri ex dicant aperiri deterruisset. Ei cum dolor pertinacia, duo veri oporteat ut. 
              Adipisci vituperatoribus ei vix, his an tale nonumes. Sea an populo intellegebat.</p>

          <p>Ex pro iisque abhorreant adversarium, partem copiosae at cum. Vim cu quem diam, nemore 
              habemus propriae vel id. Eum ne dicat nostro. Blandit intellegebat qui no, ut mea ludus 
              labitur. Eam ea assentior disputationi, qui ne eros erroribus interpretaris. Enim posse 
              assentior eam ei, quot possim placerat eum ei.</p>
        </div>                   
     
        <div class="content-wrapper">
            <div class="part">
                <h5>Specialities</h5>
                <p>description goes here...</p>
            </div>
            <div class="part">
                <h5>Skills</h5>
                <p>description goes here...</p>
            </div>
            <div class="part">
                <h5>Education</h5>
                <p>description goes here...</p>
            </div>
            <div class="part">
                <h5>Certificates</h5>
                <p>description goes here...</p>
            </div>
        </div>      
    </div>
  );
}

export default About;
