import React from "react";
import "./nav.css";
import { Link } from "react-router-dom";

class Nav extends React.Component {
  constructor(props) {
    super(props);
    this.openNav = this.openNav.bind(this);
    this.closeNav = this.closeNav.bind(this);
  }

  openNav() {
    document.getElementById("sideNav").style.width = "100%";
  }

  closeNav() {
    document.getElementById("sideNav").style.width = "0";
  }

  render() {
    return (
      <header>
        <nav id="sideNav" className="sideNav">
          <Link to="/" onClick={this.closeNav}>
            Portfolio
          </Link>
          <Link to="/about" onClick={this.closeNav}>
            About
          </Link>
          <Link to="/contact" onClick={this.closeNav}>
            Contact
          </Link>
          <Link to="/submissions" onClick={this.closeNav}>
            Submissions
          </Link>

          <button className="closeBtn" onClick={this.closeNav}>
            &times;
          </button>
        </nav>
        <div className="menu" onClick={this.openNav}>
          <div className="menu-bars">
            <div className="bar1"></div>
            <div className="bar2"></div>
            <div className="bar3"></div>
          </div>
          <div className="menu-text">Menu</div>
        </div>
      </header>
    );
  }
}

export default Nav;
