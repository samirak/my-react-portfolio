import React from "react";
import "./footer.css";

function Footer() {
  return (
    <div>
      <footer>
        <div className="footer">
          <div>
            <p>Samira Khakshoor | Full Stack Developer | Toronto</p>
            <p>&copy;2020 All Rights Reserved.</p>
          </div>
          <div className="social-icons">
            <a href="https://www.github.com/">
              <i className="fab fa-git-square"></i>
            </a>
            <a href="https://www.linkedin.com/">
              <i className="fab fa-linkedin"></i>
            </a>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
