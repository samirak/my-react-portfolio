import React from 'react';
import "./logo.css";

function Logo() {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 407.5 281">      
      <g id="Layer_4" data-name="Layer 4">
          <rect width="407.5" height="281" rx="12" fill="#330033"/>
          <rect className="cls-1" x="10.75" y="69" width="386.25" height="200" fill="#ffffff"/>
          <circle className="cls-2" cx="372.71" cy="34.29" r="15.29" fill="#cc00cc"/>
          <circle className="cls-2" cx="330.71" cy="34.29" r="15.29" fill="#cc00cc"/>
          <circle className="cls-2" cx="289.21" cy="34.29" r="15.29" fill="#cc00cc"/>
      </g>
      <g id="Layer_3" data-name="Layer 3">
          <rect x="377.01" y="321.97" width="4.82" height="74.48" transform="translate(91.74 -303.11) rotate(20)" fill="#800080"/>
          <text className="cls-3" transform="translate(71.66 188.75)">SAMIRA</text>
          <polyline points="346 199 386 167 346 135 346 143 376 167 346 191" fill="#800080"/>
          <polyline points="61.25 135 21.25 167 61.25 199 61.25 191 31.25 167 61.25 143" fill="#800080"/>
      </g>
    </svg>
  );
}

export default Logo;