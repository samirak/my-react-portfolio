# My Personal Portfolio React App

This project is a personal portfolio which was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
To fully test this application, you need to clone the backend API from [https://gitlab.com/samirak/my-react-portfolio](https://gitlab.com/samirak/my-react-portfolio).


## Installation
1. Run `npm install` to install dependencies
2. Run `npm start` to run the app in the development mode on port 3000. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
